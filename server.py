#!/usr/bin/env python

ip='62.109.14.225'

import asyncio
import websockets
import json
import string
import random

from copy import deepcopy
from qrcode import *

code=dict()

code[-1]='incoming message does not fit json format'
code[-2]='wrong message format'
code[-3]='you need to login first'
code[-4]='wrong room id'
code[-5]='unkown error'


code[3]='login success'
code[1]='message sent'

#messages are json dictionaries stored as strings
#every message must have 'type' attribute and others attributes according to specified type
types=dict()

types['message']=['type','body']
types['login']=['type','room_id']
types['sign_up']=['type']
types['technical']=['type','code','text']
types['id_respond']=['type','qr','room_id']           # need to refactor

#dict to check arguments types
inner_type=dict()

inner_type['body']=type('some message')
inner_type['room_id']=type('qw673427ds')
inner_type['code']=type(0)
inner_type['text']=type('some text')
inner_type['qr']=type('http://'+ip+'/qr/qwert12345')
inner_type['type']=type('message')

data={}  # message bank -//- key = user id value= messages list
rooms={} # key = room id, value = user id list
users={} #key = user id value = room id



def id_generator(size=16, chars=string.digits+string.ascii_lowercase):
	return ''.join(random.choice(chars) for _ in range(size))

def id_generator_from_socket(w):
	s=str(w).split(' ')[-1][0:-1]
	return s+id_generator(size=len(s))

def demodulate(incoming,user_id):
	#json check
	try:
		incoming=json.loads(incoming)
		print('incoming : ',incoming)
	except:
		answer={'type':'technical','code':-1,'text':code[-1]}
		data[user_id].append(answer)
		return False

	#format check
	try:
		if incoming['type'] in types.keys():
			for parameter in types[incoming['type']]:
				if type(incoming[parameter]) != inner_type[parameter]:
					answer={'type':'technical','code':-2,'text':code[-2]}
					data[user_id].append(answer)
					return False
		else:
			answer={'type':'technical','code':-2,'text':code[-2]}
			data[user_id].append(answer)
			return False
	except Exception as e:
		answer={'type':'technical','code':-2,'text':code[-2]}
		data[user_id].append(answer)
		return False

	return incoming


def login_stop(incoming,user_id):
	if incoming['type']=='sign_up':
		return False # you need to go further

	if incoming['type']=='login':
		if incoming['room_id'] not in rooms.keys():
			answer={'type':'technical','code':-4,'text':code[-4]}
			data[user_id].append(answer)
			return True #wrong password, get out
		else:
			answer={'type':'technical','code':3,'text':code[3]}
			rooms[incoming['room_id']].add(user_id)
			users[user_id]=incoming['room_id']

			data[user_id].append(answer)
			return True # login ok, now get outgoing
	else:
		if user_id not in users.keys():
			if incoming['type'] not in ['login','sign_up']:
				answer={'type':'technical','code':-3,'text':code[-3]}
				data[user_id].append(answer)
				return True #need to login first! gtfo
		else:
			return False # proceed, citizen

async def producer(user_id):
	if len(data[user_id])>0:
		return data[user_id].pop()
	else:
		await asyncio.sleep(1)
		return None

async def consumer(incoming,user_id):

	incoming = demodulate(incoming,user_id)
	if incoming==False:
		return

	if login_stop(incoming,user_id):
		return

	#get account and log-in
	if incoming['type']=='sign_up':
		room_id=id_generator()

		while room_id in rooms.keys():
			room_id=id_generator()

		rooms[room_id]=set([user_id])
		users[user_id]=room_id
		data[user_id]=[]

		qr = QRCode(version=1, error_correction=ERROR_CORRECT_Q) #from 1 to 40
		qr.add_data(room_id)
		qr.make()

		im = qr.make_image()
		qr=room_id+".png"
		im.save("/var/www/html/qr/"+qr)

		head='http://'+ip+'/qr/'

		answer={'type':'id_respond','room_id':room_id,'qr':head+qr}
		data[user_id].append(answer)
		return

	#send message to room
	if incoming['type']=='message':

		room=users[user_id]

		for user in rooms[room]:
			if user!=user_id:
				data[user].append(incoming)

		answer={'type':'technical','code':1,'text':code[1]}
		data[user_id].append(answer)
		return
	print("\n")
	print(incoming)
	print("\n")

	answer={'type':'technical','code':-5,'text':code[-5]}
	data[user_id].append(answer)

async def handler(websocket, path):
	user_id=id_generator_from_socket(websocket)
	data[user_id]=[]
	print(websocket,' connected')
	while True:
		listener_task = asyncio.ensure_future(websocket.recv())
		producer_task = asyncio.ensure_future(producer(user_id))
		done, pending = await asyncio.wait(
			[listener_task, producer_task],
			return_when=asyncio.FIRST_COMPLETED)

		if listener_task in done:
			message = listener_task.result()
			await consumer(message,user_id)
		else:
			listener_task.cancel()

		if producer_task in done:
			message = producer_task.result()
			if message!=None:
				print('sending : ',message)
				await websocket.send(json.dumps(message))
		else:
			producer_task.cancel()


if __name__ == "__main__":

	#start_server = websockets.serve(handler, ip, 8765)
	start_server = websockets.serve(handler, 'localhost', 8765)
	asyncio.get_event_loop().run_until_complete(start_server)
	asyncio.get_event_loop().run_forever()
