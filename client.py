#!/usr/bin/env python

import asyncio
import websockets
import json

from pprint import pprint

types=dict()

types['message']=['body']
types['login']=['room_id']
types['sign_up']=[]
types['technical']=['code','text']
types['id_respond']=['qr','id']

types['request']=[]  # wrong format but allows synch console client to recieve messages

#adress='ws://62.109.14.225:8765'
adress='ws://localhost:8765'


async def c():
	async with websockets.connect(adress) as websocket:  #wss for secure ws non secure
		while True:
			parceline={}

			t=input('enter type \n> ')
			if t=='q':
				pass
			else:
				if t in types.keys():
					parceline['type']=t
				else:
					pprint('wrong type')
					continue

				for p in types[t]:
					i=input('enter '+p+' \n> ')
					parceline[p]=i


				parceline=json.dumps(parceline)
				await websocket.send(parceline)

			answer = await websocket.recv()
			print('- '*10)
			pprint(answer)
			print('- '*10)
if __name__ == "__main__":

	asyncio.get_event_loop().run_until_complete(c())
